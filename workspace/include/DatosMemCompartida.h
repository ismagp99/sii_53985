//Autor: Ismael Gómez Pacheco 
// DatosMemCompartida.h: interface for the DatosMemCompartida class.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "Esfera.h"
#include "Raqueta.h"
class DatosMemCompartida
{
public:
	Esfera esfera;
	Raqueta raqueta1;
	int accion;
};
