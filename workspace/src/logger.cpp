//Autor: Ismael Gómez Pacheco 
//logger.cpp: implementation of the logger class.
//
//////////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include "Puntuacion.h"

int main(){
	int fd;
	Puntuacion puntos;
	
	if (mkfifo("FIFO_LOGGER", 0600)<0) {
		perror("Error al crear el FIFO");
		return 1;
	}
	
	if ((fd=open("FIFO_LOGGER", O_RDONLY))<0) {
		perror("Error al abrir el FIFO");
		return 1;
	}
	
	while (read(fd, &puntos, sizeof(puntos))==sizeof(puntos))  {
		if(puntos.anotador==1){
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}
		else if(puntos.anotador==2){
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
		if(puntos.jugador1 == 5 || puntos.jugador2 == 5){
			break;
		}
	}
	close(fd);
	unlink("FIFO_LOGGER");
	return(0);

}

