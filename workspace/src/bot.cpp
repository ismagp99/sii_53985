//Autor: Ismael Gómez Pacheco 
//bot.cpp: implementation of the bot class.
//
//////////////////////////////////////////////////////////////////////

#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <fcntl.h>

int main() {
	int fd;
	DatosMemCompartida *pDatComp;
	if((fd=open("DatComp", O_RDWR))<0) {
		perror("Error apertura fichero");
		exit(1);
	}
	if((pDatComp=static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0)))==MAP_FAILED){
		perror("Error proyección del fichero");
		close(fd);
		exit(1);
	}
	close(fd);

while(pDatComp!=NULL){

	if(pDatComp->esfera.centro.y<pDatComp->raqueta1.getCentro().y){
		pDatComp->accion=-1;
	}
	else if(pDatComp->esfera.centro.y==pDatComp->raqueta1.getCentro().y){
		pDatComp->accion=0;
	}
	else if(pDatComp->esfera.centro.y>pDatComp->raqueta1.getCentro().y){
		pDatComp->accion=1;
	}
	usleep(25000);
}

	unlink("DatComp");
	return 1;
}

