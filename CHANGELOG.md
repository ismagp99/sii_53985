# Changelog
Todos los cambios destacables en nuestro proyecto se mencionarán aquí

## [5.1] - 2020-12-15
### Añadido
- Sustituir las FIFO por sockets para una comunicación local entre cliente y servidor
- Se modifica CMakeLists.txt para que incluya la clase socket en el proceso de compilación

## [4.2] - 2020-11-27
### Añadido
- Conexión para que se puedan enviar las coordenadas del servidor al cliente
- Conexión para que se puedan enviar las teclas pulsadas del cliente al servidor

## [4.1] - 2020-11-26
### Añadido
- Duplicado de las clases Mundo.cpp, Mundo.h y tenis.cpp
- Renombrado de las clases duplicadas a MundoCliente, MundoServidor, cliente y servidor respectivamente
- Se ha modificado el CMakeLists.txt para generar los dos ejecutables servidor y cliente

## [3.2] - 2020-11-22
### Añadido
- Clase bot.cpp que permite que la máquina controle una de las raquetas y juguemos contra ella
- Actualización de otras clases para incorporar el bot.cpp y su funcionalidad
- Establecimiento del fin de partida al llegar uno de los jugadores a 5 puntos
- Actualización del fichero README informando de las novedades en eldesarrollo del juego que hemos introducido

## [3.1] - 2020-11-21
### Añadido
- Clase logger.cpp que lleve el registro de puntos
- Actualización de otras clases para incorporar al logger.cpp y su funcionalidad

## [2.1] - 2020-10-22
### Añadido
- Funciones de movimiento en Esfera.cpp y Raqueta.cpp
- Código que reduce el tamaño de la pelota con el paso del tiempo
- Fichero README que incluye las instrucciones y controles del juego
- Etiqueta fin de práctica 2

## [1.2] - 2020-10-17
### Añadido
- Etiqueta final que indica el final de la práctica 1

## [1.1] - 2020-10-08
### Añadido
- Este archivo CHANGELOG que indica los cambios que sufre el proyecto
- Una descripción en cada archivo que indica el nombre del autor
- Una etiqueta de prueba para comprobar su funcionamiento
